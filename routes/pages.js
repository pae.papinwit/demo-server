const express = require("express");
const mysql = require("mysql");

const router = express();

const mysqldb = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE,
});

// DECLARING CUSTOM MIDDLEWARE
const ifNotLoggedin = (req, res, next) => {
  if (!req.session.isLoggedIn) {
    return res.redirect("/login");
  }
  next();
};

const ifLoggedin = (req, res, next) => {
  if (req.session.isLoggedIn) {
    return res.redirect("/");
  }
  next();
};

//Home
router.get("/", ifNotLoggedin, (req, res, next) => {
  mysqldb.query(
    "SELECT * FROM `users` WHERE `id`=?",
    [req.session.userID],
    (error, results) => {
      if (error) {
        console.log(error);
      }
      res.render("home", {
        userID: results[0].id,
        name: results[0].name,
        email: results[0].email,
      });
    }
  );
});

//Login
router.get("/login", ifLoggedin, (req, res) => {
  res.render("login", { message: "" });
});

//Register
router.get("/register", ifLoggedin, (req, res) => {
  res.render("register", { message: "" });
});

//Logout
router.get("/logout", (req, res) => {
  //session destroy
  req.session = null;
  res.render("login", { message: "" });
});

module.exports = router;
