const express = require("express");
const authController = require("../controllers/auth");

const router = express();

const ifLoggedin = (req, res, next) => {
  if (req.session.isLoggedIn) {
    return res.redirect("/home");
  }
  next();
};

router.post("/register", ifLoggedin, authController.register);

router.post("/login", ifLoggedin, authController.login);

router.post("/logout", (req, res) => {
  req.session = null;
  return res.redirect("/");
});

module.exports = router;
