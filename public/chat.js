const email = document.getElementById("email").innerText;
const userID = document.getElementById("userID").innerText;
console.log(userID);
console.log(email);
let socket = io.connect();

var onlineUsers = [];

socket.on("connect", () => {
  console.log(socket.id); // x8WIv7-mJelg7on_ALbx

  socket.emit("joined-user", {
    username: email,
    id: userID,
  });
});
socket.on("send-online-users", (data) => {
  //console.log(data);
  onlineUser = data;
  console.log(onlineUser.length);
  document.getElementById("onlineUser").innerText = onlineUser.length - 1;
  GenerateOnlineList(onlineUser, userID, email);
});

let friendList = document.getElementById("friend-list");
let sendMsg = document.querySelector("#message");
let SendMsgBtn = document.querySelector("#messageBtn");
let messageList = document.querySelector("#message-list");

SendMsgBtn.addEventListener("click", (e) => {
  console.log(sendMsg.value);
  socket.emit("new_message", { message: message.value });
  message.value = "";
});

socket.on("receive_message", (data) => {
  console.log(data);
  let listItem = document.createElement("li");
  listItem.textContent = data.username + ": " + data.message;
  listItem.classList.add("list-group-item");
  messageList.appendChild(listItem);
});

let LogoutBtn = document.getElementById("logoutBtn");
if (LogoutBtn) {
  LogoutBtn.addEventListener("click", (e) => {
    socket.emit("user-disconnected", {
      username: email,
      id: userID,
    });
    document.getElementById("logoutBtnHidden").click();
  });
}
function GenerateOnlineList(AllUser, thisUserID, thisUserEmail) {
  friendList.innerHTML = "";
  let FriendlistItem = document.createElement("li");
  for (i = 0; i < AllUser.length; i++) {
    if (AllUser[i].user != thisUserEmail) {
      FriendlistItem.textContent = AllUser[i].user;
      FriendlistItem.classList.add("friend-user");
      friendList.appendChild(FriendlistItem);
    }
  }
  let friendUserList = document.getElementsByClassName("friend-user");

  for (i = 0; i < friendUserList.length; i++) {
    friendUserList[i].addEventListener("click", (e) => {
      console.log("List click test USER: " + friendUserList[i].innerText);
    });
  }
}
